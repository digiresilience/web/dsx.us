---
layout: post
title:  "November 2019 DSXpress"
date:   2019-12-02 6:00:00
author: Ingrid |
permalink: /november-2019-dsxpress/
---

This month, our team presented at [Bread & Net](https://www.breadandnet.org/unconferenceprogram.html) covering our new [Digital Wellness Check](https://gitlab.com/digiresilience/digital-wellness-check). We were pleased with the positive reception and eager to continue getting feedback as we pilot it with more organizations. Are you interested in receiving a digital wellness check for your organization? If so, please [reach out](https://dsx.us/request-assistance/)!

With the holiday season upon us, we’d like to take the opportunity to give thanks for being part of this community. We value getting to be a small part of great organizations when supporting your digital security needs. Speaking of holidays, if you’re looking to do some shopping, don’t miss the [yearly Mozilla guide](https://foundation.mozilla.org/en/privacynotincluded/) where they review the security and privacy of popular gifts.  

See below for more news and highlights.  
 


* * *


**Twitter employees spy for Saudi Arabia**  
[https://www.nytimes.com/2019/11/06/technology/twitter-saudi-arabia-spies.html](https://www.nytimes.com/2019/11/06/technology/twitter-saudi-arabia-spies.html)  

*Who?* Twitter users critical of Saudi Arabia  
*What Happened?* **Insider Threat**. Employees that use their access to company information for unauthorized purposes.  
*How?* Two Twitter employees with access to users personal data have been accused by the U.S. Justice Department of using that access to aid Saudi Arabia. The accused individuals looked into accounts of Twitter users that spoke out against the policies of the kingdom and its leaders. Information such as email addresses and phone numbers were gathered on behalf of Saudi requests.  

*What could I do?* **Threat Modeling**. It is important to fully think out all parties which might pose a risk to your operations and sensitive data. Disgruntled, compromised, or simply careless staff can often be overlooked. To learn more, visit EFF’s guide to threat modeling at [https://sec.eff.org/topics/threat-modeling](https://sec.eff.org/topics/threat-modeling).  
  
&nbsp;  

**US border warrantless searches**  
[https://nakedsecurity.sophos.com/2019/11/14/warrantless-searches-of-devices-at-us-borders-ruled-unconstitutional/](https://nakedsecurity.sophos.com/2019/11/14/warrantless-searches-of-devices-at-us-borders-ruled-unconstitutional/)  
*Who?* Travelers crossing US borders  
*What Happened?* **Constitutionality**. Policies and procedures at the border were found by a U.S. District Court to violate the Fourth Amendment of the U.S. Constitution.  
*How?* Individuals, both citizens and foreign, at U.S. borders have been subject to suspicionless searches of their devices. EFF and the ACLU brought a case of 11 such instances against the Department of Homeland Security (DHS), U.S. Customs and Border Protection (CBP), and the U.S. Immigration and Customs Enforcement (ICE). The judge ruled that, ‘reasonable suspicion’ was needed for agents to conduct these searches. She also dismissed the distinction between forensic and manual searches, stating both methods were intrusive and would require reasonable suspicion. Finally, the ruling limited the permissible scope of searches to look for, ‘digital contraband.’  

*What could I do?* **Plan Ahead**. Before any trip, think about what data is on the devices you carry with you. Offload, delete, or logout of accounts before crossing a border to minimize what information could be obtained. If you work as part of an organization, create a checklist for individuals to run through before they travel. To learn more, check out our [conference checklist](https://dsx.us/conference-tips-checklist/) for a list of items to consider.  
&nbsp;  

  
 
**Bluetooth scanners aid thieves**  
[https://www.wired.com/story/bluetooth-scanner-car-thefts/](https://www.wired.com/story/bluetooth-scanner-car-thefts/)  

*Who?* Individuals that leave electronics in their cars  
*What Happened?* **Bluetooth Scanners.** Using readily available phone apps or scanners, anyone can locate electronics hidden in a vehicle.  
*How?* The story details how devices that are on, closed, or idle can be found via bluetooth scanners.  Such devices can be used by criminals to know which cars have valuable hidden gadgets for the taking.  

*What could I do?* **Power Down**. If possible, don’t leave electronics unattended in a car. If you must, ensure the devices are powered off and/or have bluetooth switched off. Also, be cautious of what nearby folks could see if you obviously place an item in your trunk.  

  
<br>

  
* * *
  

  
**New App to Detect iPhone Hack**  
A new app, iVerify, [claims](https://www.vice.com/en_us/article/bjw474/this-app-will-tell-you-if-your-iphone-gets-hacked-iverify) to help detect compromises on iPhones. The app further includes security and privacy advice to users.  

**Data Brokers Opt Out**  
[This](https://www.wired.com/story/opt-out-data-broker-sites-privacy/) Wired article outlines ways you can actively work to remove your information from data brokers and work to enhance your privacy.  

**I Let a Hacker Hack Me**  
A tech report asked a hacker to hack him, and she did with great success. Read the [article](https://edition.cnn.com/2019/10/18/tech/reporter-hack/index.html) to see how she did it and what tips you can learn to better protect yourself.  

**Security Professionals end up in Jail**  
Due to differing interpretation of a contract and a disagreement between State and County, two security professionals conducting a physical security assessment were placed in jail. [Read](https://arstechnica.com/information-technology/2019/11/how-a-turf-war-and-a-botched-contract-landed-2-pentesters-in-iowa-jail/) how it happened and what steps might be taken to protect future engagements.  
<br>

* * *
  

    

Tech Tips
---------
  
**Internet Shutdown**. When a specific region has their access to the internet severely limited or completely shut off, they are experiencing what’s called an Internet Shutdown. Highlighted in our [August post](https://dsx.us/august-2019-dsxpress/), Kashmir experienced such an event. Just this year, we’ve also seen shutdowns in [Zimbabwe](https://www.aljazeera.com/news/2019/01/zimbabwe-imposes-total-internet-shutdown-crackdown-190118171452163.html), [Sudan](https://www.hrw.org/news/2019/06/12/sudan-end-network-shutdown-immediately), and most recently, [Iran](https://thenextweb.com/tech/2019/11/18/iran-shut-down-citizens-internet-access-following-protests-over-fuel-pricing/).  
<img src="../assets/img/shutdown.jpg" width="420" height="420" alt="Image">  


Mesa, Jose [Online Image]. Retrieved November 25, 2019, from [https://www.flickr.com/photos/liferfe/5412934281/](https://www.flickr.com/photos/liferfe/5412934281/)

These shutdowns often aim to quell protests and limit media coverage. Shutdowns vary in their scale and implementation, so it might be possible to get some methods of communication, such as text message, while others, like WiFi, won’t work. As always, be wary of what downloads and applications you use in such events and look to trusted groups for guidance. 

Want to learn more? Visit [https://netblocks.org/](https://netblocks.org/) and [https://www.accessnow.org/keepiton/](https://www.accessnow.org/keepiton/) to read the latest and learn how you can do your part to contribute to detecting them.