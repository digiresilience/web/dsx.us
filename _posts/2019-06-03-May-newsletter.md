---
layout: post
title:  "May 2019 DSXpress"
date:   2019-06-03 9:00:00
author: Ingrid |
permalink: /may-2019-dsxpress/
---

As we look forward to meeting with current and future friends and partners next month at [RightsCon](https://rightscon.org/), we’re busily moving a few things forward. We’re coordinating with partners to make accessibility adaptations to the SAFETAG security auditing framework, which is used by many security trainers working in human rights; we’re growing our provider network through a number of exciting partnerships; and we’re of course continuing to take in new requests for needs assessments and trainings.

You can reach out directly for assistance by emailing us at help@dsx.us, sending us a note on Signal at 413-629-8478, or filling out [this form](https://dsx.us/request-assistance-form/).

See below for more news and highlights.  
 

Upcoming Events
---------------


  
Come meet us! Upcoming events where you can personally get to know DSX:

[RightsCon](https://www.rightscon.org/) June 11-14, Tunis, Tunisia 

* * *


**Cyber Attacks Against Philippine Organizations**  
[https://www.qurium.org/press-releases/disclosing-company-that-facilitated-massive-cyber-attacks-against-20-regime-critical-philippine-organizations/](https://www.qurium.org/press-releases/disclosing-company-that-facilitated-massive-cyber-attacks-against-20-regime-critical-philippine-organizations/)

*Who?*: Media outlets and civil society organizations in the Philippines  
*What Happened?*: **Distributed Denial of Service (DDoS)**. When a malicious entity overwhelms a website with an enormous amount of traffic in an effort to make the site and services unavailable. Brian Krebs has a number of blog [posts](https://krebsonsecurity.com/tag/ddos/) enumerating various historical attacks.  
*How?*: A malicious actor, going by the nickname P4p3r, was allegedly given a list of websites to attack. The list contained several independent media outlets, human rights organizations, political parties, and journalist associations. The actor used a number of techniques to DDoS the targeted websites.

*What could I do?*

**Proper Configuration and Vendor Services.** Secure setup of servers and websites are foundational steps to help protect organizations. Beyond that, several companies offer services to add enhanced DDoS protection, such as Google’s [Project Shield](https://projectshield.withgoogle.com/landing). To learn more, visit [https://www.cloudflare.com/learning/ddos/ddos-mitigation/](https://www.cloudflare.com/learning/ddos/ddos-mitigation/)

  
  
**Two Vulnerabilities: Titan Security Keys and WhatsApp**  
[https://security.googleblog.com/2019/05/titan-keys-update.html](https://security.googleblog.com/2019/05/titan-keys-update.html)  
[http://time.com/5588733/whatsapp-spyware-israel/](http://time.com/5588733/whatsapp-spyware-israel/)

*Who?*: Users of Titan Security Keys, WhatsApp Users  
*What Happened?*: **Vulnerabilities.** The reason software updates are important is new vulnerabilities are discovered all the time.  
*How?*: For Titan Security Keys, an issue was discovered in the way it used Bluetooth. In order for the vulnerability to be exploited, a malicious user would have to be in close proximity to the security key (within 30 feet) when it was being used and would need to know the username and password for the targeted individual.  
  
For WhatsApp, a compromise was discovered that allowed unauthorized users access to a users phone by simply placing a WhatsApp call to it. The compromise could occur without any clicks or steps taken by the targeted user.

*What can I do?*:  
**Software Update and Key Replacement.** For the Titan Security Keys, follow the instructions in the link above to determine if you are impacted and how to go about requesting a replacement. Want to learn more about the different types of hardware security keys? Visit: [https://www.theverge.com/2019/2/22/18235173/the-best-hardware-security-keys-yubico-titan-key-u2f](https://www.theverge.com/2019/2/22/18235173/the-best-hardware-security-keys-yubico-titan-key-u2f)

For the WhatsApp vulnerability, ensure you and your contacts are updated to the latest software version.  
  
  
**Open Source IoT Tool**  
[https://techcrunch.com/2019/04/13/spy-on-your-smart-home-with-this-open-source-research-tool/](https://techcrunch.com/2019/04/13/spy-on-your-smart-home-with-this-open-source-research-tool/)

*Who?*: People with internet-connected products (Smart TVs and thermostats, voice assistants) in their homes.  
*What does it do?*: **Provide Information.** The Princeton IoT Inspector provides a utility for less tech savvy individuals to see who their devices are talking to.   
*How?*: Princeton University researchers have developed this tool for individuals that don’t feel comfortable using more technically advanced means, such as WireShark, to look at network traffic. The tool looks at several data points related to network traffic to analyze who the device is talking to and how often.

*Can I use it?*:  
**Yes!** The download for the tool is available [here](https://iot-inspector.princeton.edu/blog/post/getting-started/). Note that any network traffic collected will be shared with researchers at Princeton University. A comprehensive [FAQ](https://iot-inspector.princeton.edu/blog/post/faq/) is available to answer questions such as what data is collected, how it is anonymized, and what it will be used for.  
 

* * *

**Git**  
Earlier this month, a new [target](http://www.zdnet.com/article/a-hacker-is-wiping-git-repositories-and-asking-for-a-ransom/) for Ransomware emerged. Github also announced two new features, [maintainer security advisories](http://help.github.com/en/articles/about-maintainer-security-advisories) and [sponsors](https://github.com/sponsors). With maintainer security advisories, users may privately draft notices and work on vulnerability fixes before being ready to push final versions out to the community. With sponsor, open source projects and their developers will have a new way to accept funding.

  
**Useful Research**  
Two great reports were published this month. First, the Verizon [2019](https://www.securityweek.com/verizon-publishes-2019-data-breach-investigations-report-dbir) Data Breach Investigations Report (DBIR). The DBIR has become a useful yearly publication to read about trends in security incidents. Google also published a [report](https://security.googleblog.com/2019/05/new-research-how-effective-is-basic.html) this month detailing how effective basic security hygiene is against account compromise.

Tech Tips
---------


Online Harassment. Doxxing, Cyber Bullying, Threats. Harassment is not anything new to society, however the internet and use of social media has amplified its effect. Women and minorities face a disproportionate amount of harassment online. It is important for our community to be aware of these acts of aggression and to properly equip vulnerable organizations and individuals to withstand these attacks.

Want to learn more? Visit Martin Shelton's list of [Current Digital Security Resources](https://medium.com/@mshelton/current-digital-security-resources-5c88ba40ce5c) and scroll down to the, 'Resources for harassment and abuse' section.