---
layout: post
title:  "2018 Year in Review"
date:   2018-12-18 9:00:00
author: Josh |
permalink: /2018-review/
---

Thanks to the support of folks like you, the DSX project has continued to grow since our official launch in March of this year, serving more and more organizations and growing our network of digital security providers. As we rapidly approach the end of 2018, I wanted to take a moment to summarize the progress and future of the DSX.


## Cases

We officially launched the DSX website and opened our doors to the public in [March of 2018](https://digitalsecurityexchange.org/welcome-dsx/). Since then, we’ve conducted more than 26 needs assessments, matched 20 cases to a provider and completed 11 cases. We’ve only gotten busier as the year has come to a close and look forward to what the new year holds.

Meanwhile, we conducted some [case studies](https://digitalsecurityexchange.org/case-studies/) that shed light on the kinds of organizations that are coming to the DSX for help:

"Early on we discovered that most groups that come to us can be loosely described as 'intermediaries': Legal aid providers, rights advocates, social service groups, and organizers working with – but usually not within – 'frontline' communities affected by issues including women’s rights, reproductive justice, domestic violence, immigrant rights, human rights, climate change, and journalism/media."

...And what kind of help we’re providing:

"... many of the organizations [asking for assistance](https://digitalsecurityexchange.org/request-assistance/) are unsure of what they actually need. It’s our goal to understand their needs and threats as much as possible before offering support. To do this, one of our first steps in the process is to meet with one of the organization’s members and conduct an initial needs assessment. From this, we are able to outline a plan forward and to ensure a better match to one of our digital security experts within our provider network."


## How we work

The above description still mostly holds, with one important caveat: We’re finding that 99% of the organizations that connect to us are seeking what you might call a “holistic organizational needs assessment,” meaning that while most orgs are not asking for -- or able to spend resources on -- a full organizational audit, they typically want a similar suite of services:

- A better understanding of the threats they face
- Help crafting the policies and practices they need to embrace
- Assistance with developing and maintaining good habits
- A way to receive and share information about current threats and best practices

To suit these needs, the DSX is often focused on connecting organizations to multiple providers, each of whom can address specific needs. For example, Provider Jane can conduct staff interviews and provide a basic digital security policy and checklist; while Provider John can assist by training staff on installing and using using new software, setting up two-factor authentication, and best practices on traveling across borders.


## Center for Digital Resilience

This approach mirrors that of our new parent organization, [Center for Digital Resilience](https://digiresilience.org/), which I co-founded earlier this year, and for which I’m now Chief Technology Officer. CDR, as we call it, works with local partners to build DSX-like projects in different regions or across different networks, in order to facilitate digital security capacity-building among cohorts of organizations. For now, CDR operates two projects -- the DSX in the U.S. and a similar project in the Middle East region -- but our goal is to begin building more communities in the coming year. If you’re interested in developing a CDR community, contact us at [info@digiresilience.org](mailto:info@digiresilience.org).

CDR is also looking for a [Security Director](https://docs.google.com/document/d/1-Ajn-tglbt9nHpk8lEFbm7WOBtpQhsyAN9v-fAWT8U8/edit). Please get in touch if you’d like to learn more.


## Ticketing and triage

You may remember that one of the DSX’s first endeavors was to build a secure system for receiving requests for assistance and matching them to the appropriate providers. We developed a couple of iterations before landing on the open-source [Zammad](https://zammad.org/) platform, which enables multi-channel communications with the DSX.

Thanks to Zammad’s built-in functionality and a few custom-built extensions, we’ve made it easy for organizations and providers to connect with us via:

- Email: [help@dsx.us](mailto:help@dsx.us)
- Twitter: DM us at [@the_DSX](https://twitter.com/the_DSX)
- Signal: +1 413-629-8478
- Our website: Click “Get Help” at [https://digitalsecurityexchange.org](https://digitalsecurityexchange.org)  

Any of these methods will create a ticket that enables us to securely track cases from start to finish -- while only retaining the data that we absolutely need to retain, stored on servers that we control. We’re putting the finishing touches on this system now and look forward to helping other communities set up their own instances!


## Community and information sharing

Nearly every organization we work with has asked for more information about threats facing them and other U.S. civil society groups, along with current news on best practices and a way to connect to other organizations directly.

Our revised newsletter -- which we’re calling DSXpress -- is one attempt at addressing this need. However, it doesn’t solve the problem of helping organizations talk to each other, and to providers, without anyone getting in the way.

In the new year we’ll be investigating the best ways to support this kind of collaboration and information sharing. If you have ideas about the best ways to do it -- tools and platforms to use, ways to promote participation -- please let us know by sending a note to [help@dsx.us](help@dsx.us) or sending a Signal message to +1 413-629-8478.


## Expansion and support

Our goal is to (slowly) scale up the number of groups we’re able to work with in 2019. Doing so will require additional resources to allow us to manage more cases for more orgs, and to identify more security providers who can help.

We work with groups that support immigrants, fight for gender justice, push back against climate change, fight corruption, and protect our human rights. Yet despite the immediacy of these issues, security experts with the necessary cultural sensitivity and background to do this work are but a small subset of the broader “cybersecurity” community. For this reason, if we’re going to continue to meet the growing needs of U.S. groups, we must work hard to grow our network of providers. **If you or anyone you know is interested in providing digital security services, please [be in touch](mailto:help@dsx.us)**.

In addition, working with more groups to provide holistic assessments will require more internal capacity on DSX’s side, including more “case managers” and a day-to-day lead to manage the overall project.

We’re working on a number of fronts to raise the funds to enable this expansion. If you have any ideas for sources of support or would like to support the project directly, contact us at [help@dsx.us](mailto:help@dsx.us). Center for Digital Resilience acts as the DSX’s fiscal sponsor, and will receive any contributions and directly pass them on to the DSX.

Whew! It’s been a busy year! We thank you for the continued support and encouragement as we continue seeking to serve U.S. groups and other communities through Center for Digital Resilience. Have a wonderful holiday season! See you in 2019!
