---
layout: post
title:  "August 2019 DSXpress"
date:   2019-09-02 9:00:00
author: Ingrid |
permalink: /august-2019-dsxpress/
---

Happy start of the school year! With the kids going back to the classroom, we thought we’d share a special blog post with contributions from one of our Cyber Squad members. Visit [here](https://dsx.us/cyber-squad-interview) to read some of the security and privacy challenges facing our schools.


See below for more news and highlights.  
 


* * *


**Kashmir Blackout**  
[https://www.totaltele.com/503591/Kashmirs-telecoms-blackout-When-critical-infrastructure-becomes-a-weapon](https://www.totaltele.com/503591/Kashmirs-telecoms-blackout-When-critical-infrastructure-becomes-a-weapon)  

*Who?* Kashmir  
*What Happened?* **Internet Blackout.** A blackout happens when a country's access to the internet is completely cut, preventing people from getting online.  
*How?* When a government wants to control messaging around an event and/or sever the free flow of information, they sometimes cut their citizens’ access to social media or the internet, at large. They do this by ordering the internet service providers (ISPs) to limit access for their subscribers.  
  
*What could I do?* Unfortunately, not much. Internet shutdowns are effective because they cut off the ability for citizens to talk to the rest of the World. In some cases, some services will still be operational, which can allow a few avenues for communication.  

To learn more, visit: [https://www.accessnow.org/keepiton/](https://www.accessnow.org/keepiton/)  
&nbsp;  

  

**GDPR Implementation Mistakes**  
[https://www.bbc.com/news/technology-49252501](https://www.bbc.com/news/technology-49252501)  

*Who?*  Companies needing to comply with the General Data Protection Regulation (GDPR)  
*What Happened?* **Poor Process.** In an effort to comply with the new regulations, some companies had to implement new processes and procedures. In doing so, weaknesses were introduced to how some companies handle data subject requests.  
*How?* Individuals are empowered under the GDPR to make certain requests to companies which hold data about them. Before a company can release such data, they have to ensure the individual making the request is who they claim to be. As the researcher showed in the link, above, he was able to obtain data about someone other than himself when companies didn’t have the right protocols in place. Such negligence could lead to abuse by stalkers, victims of domestic abuse, and more.  

*What could I do?* As a company, you can research proper steps to take, join forums and conferences to connect with and learn from your peers, or get advice from outside consultants.  

To learn more, visit [https://iapp.org/news/a/how-to-verify-identity-of-data-subjects-for-dsars-under-the-gdpr/](https://iapp.org/news/a/how-to-verify-identity-of-data-subjects-for-dsars-under-the-gdpr/)  
&nbsp;  

  
 
**ProtonMail Phishing Campaign**  
[https://www.bellingcat.com/news/uk-and-europe/2019/08/10/guccifer-rising-months-long-phishing-campaign-on-protonmail-targets-dozens-of-russia-focused-journalists-and-ngos/](https://www.bellingcat.com/news/uk-and-europe/2019/08/10/guccifer-rising-months-long-phishing-campaign-on-protonmail-targets-dozens-of-russia-focused-journalists-and-ngos/)  

*Who?* Russia-Focused Journalists and NGOs  
*What Happened?* **Phishing**  
*How?* A malicious group set up fake domains and sent sophisticated phishing messages to ProtonMail users indicating some concern about activity on their account. One such message even used an internal ProtonMail server, enabling end-to-end encryption for the message. Another advanced method used by the group involved mixing both real ProtonMail links in the phishing message with the fake, malicious ones.  

*What could I do?* Adversaries are getting more clever in crafting phishing emails. One good step to take is whenever a message comes through warning about activity on an account, you can navigate straight to the account on your own rather than using the links embedded in the email.  

To learn more, visit [https://www.consumer.ftc.gov/articles/how-recognize-and-avoid-phishing-scams](https://www.consumer.ftc.gov/articles/how-recognize-and-avoid-phishing-scams)  

  
<br>

  
* * *
  

  
**MFA FTW**  
A new [article](https://www.zdnet.com/article/microsoft-using-multi-factor-authentication-blocks-99-9-of-account-hacks/) from Microsoft shares that using multi-factor authentication (MFA) blocks 99.9% of account hacks.  

**VPN Review**  
Wirecutter shared an updated [guide](https://thewirecutter.com/reviews/best-vpn-service/) reviewing several VPN providers and making recommendations on which came out on top.  
  
**NYC Partners with Hackers to Fight Stalkerware**  
[https://www.technologyreview.com/s/614168/nyc-hires-hackers-to-hit-back-at-stalkerware/](https://www.technologyreview.com/s/614168/nyc-hires-hackers-to-hit-back-at-stalkerware/)


<br>

* * *
  

    

Tech Tips
---------
  


**Zero Days**. A recent bombshell [report](https://googleprojectzero.blogspot.com/2019/08/a-very-deep-dive-into-ios-exploit.html) by Google’s Project Zero team has brought the topic of ‘0-days’ into the forefront. Zero days refer to both a vulnerability and a corresponding exploit in a device, application, or other piece of computer code for which there is no published security fix. There is some debate around whether the ‘zero’ refers to the date the exploit is first used (as shown in the diagram, below) or the date the public and/or vendor first learn of the vulnerability and corresponding exploit. 
  
<img src="../assets/img/zeroday.png" width="500" height="250" alt="Image">  
*[Source](https://upload.wikimedia.org/wikipedia/commons/3/3b/Winodw.gif) Jelois [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)]*

[Industries](https://www.zerodium.com) even exist that actively look to purchase said exploits. Zero days are particularly challenging to protect against. Since they are so powerful, they are often only used for high-value, specific targets. Basic security hygiene measures such as using two-factor authentication and keeping an eye out for suspicious behavior can still be useful tools for protection. Most importantly, as soon as a vendor does release a corresponding security patch it is critical to make time to install it. 

Want to learn more? [https://www.wired.com/2014/11/what-is-a-zero-day/](https://www.wired.com/2014/11/what-is-a-zero-day/)  