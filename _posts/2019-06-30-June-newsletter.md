---
layout: post
title:  "June 2019 DSXpress"
date:   2019-06-28 9:00:00
author: Ingrid |
permalink: /june-2019-dsxpress/
---

We’re back from RightsCon 2019 in Tunisia! It was a great, great conference, with nearly 2,500 human rights and technology advocates from dozens of countries discussing the human rights implications of AI, data protection policy, supporting social movements online, and, of course, digital security best practices. 

We got a lot out of it, including the opportunity to share the CDR Link helpdesk platform (read more about it [here](https://tech.digiresilience.org/2019/05/30/what-is-CDR-link.html)) and the chance to meet with friends old and new who are working to protect human rights activists from online attack.

Since we’re still in the midst of conference season madness, we put together this handy guide to protecting yourself online while traveling to events: [https://dsx.us/conference-tips-checklist/](https://dsx.us/conference-tips-checklist/) 

Check it out, bookmark it, and print it out before you head to the next event! 

See below for more news and highlights.  
 


* * *


**Snooping Soccer App**  
[https://www.theverge.com/platform/amp/2019/6/12/18662968/la-liga-app-illegal-soccer-streaming-fine](https://www.theverge.com/platform/amp/2019/6/12/18662968/la-liga-app-illegal-soccer-streaming-fine)

*Who?* Soccer fans with the LaLiga app installed on their devices  
*What Happened?* **Unauthorized Permissions**. The app was using the devices to collect data in ways outside what users understood and expected.  
*How?* The app used location data and the microphone on devices where it was installed to record its surroundings. This collection of data allowed the app to determine if establishments soccer fans watched matches at were broadcasting games without having a license.

*What could I do?*

Regularly review the permissions granted to your device apps to see if they make sense. You can always start with denying most permissions and then go back and enable the ones you need. To learn more, visit [https://freedom.press/training/your-smartphone-and-you-handbook-modern-mobile-maintenance/](https://freedom.press/training/your-smartphone-and-you-handbook-modern-mobile-maintenance/) and navigate down to the section, "Is an app asking you for reasonable permissions?"

  
  
**Hong Kong Protests**  
[https://www.washingtonpost.com/world/asia_pacific/masks-cash-and-apps-how-hong-kongs-protesters-find-ways-to-outwit-the-surveillance-state/2019/06/15/8229169c-8ea0-11e9-b6f4-033356502dce_story.html](https://www.washingtonpost.com/world/asia_pacific/masks-cash-and-apps-how-hong-kongs-protesters-find-ways-to-outwit-the-surveillance-state/2019/06/15/8229169c-8ea0-11e9-b6f4-033356502dce_story.html)   
[https://www.newstatesman.com/world/asia/2019/06/youth-hong-kong-are-offering-masterclass-how-protest](https://www.newstatesman.com/world/asia/2019/06/youth-hong-kong-are-offering-masterclass-how-protest)  

*Who?* Protesters in Hong Kong  
*What Happened?* **Securely Attending a Protest.** Best practice tips on protecting electronic devices and practicing smart habits when part of a protest.  
*How?* Protesters took several steps to reduce and protect their digital footprints. They used secure, encrypted communications, avoided taking pictures of each other, and used anonymous sources of payment (cash) for transit tickets, to name a few. Tips on how to protect the activists were passed around and shared on messaging apps. All these measures are meant to keep protesters safe and avoid surveillance and punishment from those in power.
  
*What can I do?*  
If you are attending a protest, use a resource like Security First’s [Umbrella](https://secfirst.org/umbrella/) app or EFF's [Attending a Protest](https://ssd.eff.org/en/playlist/activist-or-protester#attending-protest) guide to be aware of what steps you should take to best protect yourself.
  
  
**SIM Hijacking**  
[https://www.vice.com/en_us/article/j5bpg7/sim-hijacking-t-mobile-stories](https://www.vice.com/en_us/article/j5bpg7/sim-hijacking-t-mobile-stories)

*Who?* Mobile Phone Users  
*What Happened?* **Vulnerability used for Social Engineering.**   
*How?* Unauthorized individuals were able to exploit a vulnerability in mobile devices and retrieve information off the device. One of these pieces of information was then able to be told to mobile provider support staff in order to issue a new SIM card for the targeted phone number. Since phone numbers are used for many sensitive things, such as two-factor authentication codes, this scheme allowed additional damage to users such as to their bank accounts and social media presence.

*What can I do?*  
Ensure you have a PIN or passcode setup on your mobile device to protect yourself from this type of attack. The last paragraph of the [story](https://www.vice.com/en_us/article/j5bpg7/sim-hijacking-t-mobile-stories) above offers some advice for how to do this, as well as this [article](https://www.techrepublic.com/article/pro-tip-protect-your-android-sim-card-with-sim-pin-lock/). Furthermore, where possible do not use phone number SMS for two factor authentication when more secure options, such as an authenticator app or hardware key, exist.
 
  
  
* * *
  
  
**Malware Protection** 
A great new [article](https://source.opennews.org/articles/shields-get-your-malware-shots/) from Martin Shelton outlining how users can better protect themselves from malicious software.

**Confidential Mode for GSuite**
Beginning June 25th, [confidential mode](http://nakedsecurity.sophos.com/2019/06/03/g-suite-users-will-have-confidential-gmail-mode-set-to-on-by-default/) will be turned on by default for all GSuite users. This feature has been available to users since April 2018, and EFF [highlighted](https://www.mic.com/p/gmails-confidential-mode-isnt-as-private-as-it-seems-according-to-experts-18136277) some concerns about how the feature works and why the name might be misleading.
  
**Guardian Firewall**  
The team set out to develop a firewall [app](https://guardianapp.com/blog/2019/06/introducing-guardian-firewall-for-ios/) that would allow users to, ‘set it and forget it.’ The app will be available for download in July.

  
* * *
  

Tech Tips
---------


**Privacy**. What does it entail? Why is it considered important? Professor Roger Clarke [suggests](http://www.rogerclarke.com/DV/Privacy.html) the importance of privacy has several different dimensions:
* **Psychologically**, people need private space.
* **Sociologically**, people need to be free to behave, and to associate with others, subject to broad social mores, but without the continual threat of being observed.  
* **Economically**, people need to be free to innovate.
* **Politically**, people need to be free to think, and argue, and act. Surveillance chills behaviour and speech, and threatens democracy.  

In his latest blog [post](https://idlewords.com/2019/06/the_new_wilderness.htm), Maciej Ceglowski writes about, ‘ambient privacy’ and its erosion in the current digital world. All this leaves people grappling with questions like, what does private browsing mode actually do to protect me? Why would I use Tor? How does that shoe ad follow me across devices? As the tweet, below, shows, it can be difficult to balance the use of the latest tech with preservation of one's privacy.


<img src="../assets/img/wiretap.png" width="275" height="400" alt="Image">

Want to learn more? Peruse a collection of privacy related [stories](https://www.nytimes.com/spotlight/privacy-project-personal-information) by the New York Times, read more about [private browsing](https://spreadprivacy.com/is-private-browsing-really-private/), and head over to EFF to learn more about [Tor and HTTPS](https://www.eff.org/pages/tor-and-https).