---
layout: post
title:  "October 2019 DSXpress"
date:   2019-11-02 6:00:00
author: Ingrid |
permalink: /october-2019-dsxpress/
---

In the U.S., the DSX’s work presses on as we continue to engage with more organizations. In the last month, we’ve interfaced with groups working to support immigrants, gender equality, reproductive rights, and victims of online harassment. Be on the lookout next month for our 2019 year in review where we’ll recap the organizations we’ve helped through highlights and case studies. As always, the information we share will be anonymized in order to respect privacy and help protect identities.  

As we work to best support these amazing organizations, we continue to connect with more service providers eager to provide them with digital security assessments, training, and guidance. [Let us know](https://dsx.us/become-a-provider/) if you are interested in joining our community yourself!

As always, feel free to **forward this newsletter** to anyone who might be interested in updates on the digital security landscape, or who is looking for assistance for their organization. Interested in learning more about our helpdesk, CDR Link? Watch [this](https://digiresilience.org/tech/2019/10/10/link-video.html) short video to hear why we think it’s so great.  

See below for more news and highlights.  
 


* * *


**VPN Hacked**  
[https://www.tomsguide.com/news/nordvpn-torguard-admit-being-hacked-but-thats-just-the-beginning](https://www.tomsguide.com/news/nordvpn-torguard-admit-being-hacked-but-thats-just-the-beginning)  

*Who?* VPN users of NordVPN, TorGuard, and VikingVPN  
*What Happened?* **Hacking**. Unauthorized intrusion into a company’s system.  
*How?* One of NordVPN’s data centers was compromised via an exploited vulnerability. Most importantly, private encryption keys were stolen for all three companies. With those keys, a fake VPN server could be setup impersonating any of the companies and used to monitor user data as it flowed through.  

*What could I do?* **Education**. When we put our data in the hands of a company, we are trusting them to protect it. Making informed choices on what products we use and which companies we trust is important. To learn more about how VPNs work, why we use them, and how to pick a good one, see our Tech Tips section below.  
&nbsp;  

  

**NSO Group and WhatsApp**  
[https://citizenlab.ca/2019/10/nso-q-cyber-technologies-100-new-abuse-cases/](https://citizenlab.ca/2019/10/nso-q-cyber-technologies-100-new-abuse-cases/)  

*Who?* Targeted WhatsApp Users  
*What Happened?* **Hacking**. Unauthorized intrusion into individual’s devices.  
*How?* As we shared in our [May](https://dsx.us/may-2019-dsxpress/) update, WhatsApp had a vulnerability that was identified and quickly fixed. This month, WhatsApp announced public attribution to the NSO Group for using this vulnerability to attack individuals. Citizen Lab assisted WhatsApp to help identify cases where the suspected targets of the attack were members of civil society, such as human rights defenders and journalists. In the article above they identify over 100 cases of such abusive targeting in at least 20 countries across the globe.  

*What could I do?* **Reach out**. When you see or notice a vulnerability, follow the recommended steps. If you aren’t sure what to do or believe you need assistance with an incident, contact DSX via our [helpdesk](https://dsx.us/request-assistance/) to get help.  
&nbsp;  

  
 
**FBI Warns of Multi-Factor Authentication Attacks**  
[https://www.zdnet.com/article/fbi-warns-about-attacks-that-bypass-multi-factor-authentication-mfa/](https://www.zdnet.com/article/fbi-warns-about-attacks-that-bypass-multi-factor-authentication-mfa/)  

*Who?* Multi Factor Authentication (MFA) Users  
*What Happened?* **SIM swapping**  
*How?* The FBI highlighted several examples where malicious users employ techniques to compromise users of MFA. When hackers conduct a SIM swap, they manage to take an individual’s phone number and port it onto a new device. This is usually done by convincing the mobile provider that the hacker is the owner of the number. Once they have switched the number to a device they control, they can gain access to text based authentication codes. Other methods listed in the notice include manipulated web addresses and monster-in-the-middle (MiM) attacks.  

*What could I do?* **Security Keys**. For MFA, use the strongest method of authentication allowed. Where possible, that means a security key. As covered in a recent Google blog [post](https://security.googleblog.com/2019/05/new-research-how-effective-is-basic.html), “zero users that exclusively use security keys fell victim to targeted phishing during our investigation.”  

  
<br>

  
* * *
  

  
**Safe Sisters**  
Fantastic [resources](https://safesisters.net/resources/) for anyone looking to become a digital security trainer or become more informed and take online safety into their own hands.  

**Google Maps Incognito Mode**  
Early this month, Google [announced](https://www.zdnet.com/article/google-launches-incognito-mode-for-google-maps-and-more/) incognito mode will be now available for Google Maps for Android, with iOS coming soon.  

**GSuite**  
Ever wondered what concerns you should have using Google for your non-profit operations? Thanks to some work by our friends at FPF, now you can [read](https://freedom.press/training/blog/newsrooms-lets-talk-about-gsuite/) about the relevant security and privacy considerations.  

<br>

* * *
  

    

Tech Tips
---------
  

**VPN**. VPNs (Virtual Private Networks) allow users to access blocked websites, hide their true location while browsing, and protect their browsing activity from snooping while using public or untrustworthy WiFi connections. However, VPNs do not provide perfect security, as evidenced by the highlighted article above. Today, internet users are faced with more options for VPN services than ever before. Although many VPN providers claim in their advertising that they can guarantee one-hundred percent security of their customers in the event of such hacking attempts, these claims can often be false and misleading. The following guidelines can assist as you consider your options, and your security when it comes to VPN services.    
<img src="../assets/img/whyEncrypt.jpg" width="620" height="350" alt="Image">  

**When should I use a VPN?**  
VPN services work by routing your network traffic through their own servers via an encrypted connection before it reaches the wider internet; therefore, your traffic appears to originate from them instead of you. This is why it is recommended to use VPNs in public places and when using untrusted WiFi to better secure your internet activity from unwanted snooping. VPNs can also be useful at times when you want to appear to be working in a different location (for example, when you are in the US but want your internet activity to appear to be in the EU). In these instances, using a VPN can help protect your privacy to a certain extent. However, given certain security and legal issues with VPN providers, such as those recently reported above, they do not offer perfect protection. Basic security practices and precautions should still be exercised while using them, including being mindful of phishing links and suspicious websites you may come across.  
    
**Does a VPN make me anonymous?** 
It is important to note that, while VPNs can help you protect your internet traffic and, in some cases, circumvent access restrictions to websites, they do not make you anonymous on the Internet. Although your Internet Service Provider (ISP), network administrator, or anyone else monitoring your traffic may only see your connection to a VPN, your identity is visible to your VPN provider, who can still see (and in many cases, keep records of) your onward connections and activity online. For more protection, while browsing the Internet and to remain anonymous, Tor Browser is currently the best option.  

**How can I tell if a VPN service is secure and reliable?**  
In the end, choosing a reliable and secure VPN provider is an important issue that should be seriously considered. Aside from the claims made in a given VPN provider's advertising, be sure to do research on the company itself to see if they’ve had any past incidents of leaks, hacking, or other compromising activity, as well as read reviews of the service from trusted sources.  

If you are using or planning on using a certain VPN and you want to know more about its security, contact us!
