---
layout: post
title:  "Cyber Squad Interview"
date:   2019-09-01 9:00:00
author: Ingrid |
permalink: /cyber-squad-interview/
---

Fall is fast approaching and with it, the start of a new school year in the US. DSX Cyber Squad member Nick is a Digital Learning Specialist with a US capital city public school system. He is acutely aware that the technical issues facing our broader society are also having a real impact on our educational institutions and students. His team is spearheading efforts to keep administrators and faculty informed on these fast evolving topics with serious security and privacy repercussions. Below Nick shares his insight around these important issues.  
&nbsp;  
&nbsp;  

**DSX: As the school year ramps up, we know many teachers are getting their classrooms ready and with that, working to figure out the various hardware systems and programs they will use in their curriculum. Can you share some of the pitfalls you’ve seen in school staff working to set up and administer some of these new technologies?**  
&nbsp;  

**Nick**: It is great to see schools and teachers harnessing available technologies for the K12 space. There are so many new ways to approach teaching and learning that the walls of the classroom are virtually disappearing. Learning now is nothing like learning was when I was a kid thirty years ago. Technology is no longer just something you use in education - it is an indispensable part of education.
&nbsp;  

That being said, we see a lot of opportunistic education technology vendors coming through our door. My team of digital learning specialists advises schools on the different applications being offered to them - “advises” being the operative word. We often see red flags in the usefulness factor, and more importantly, in the privacy and security of the application the vendor is promoting. We inform schools on our findings and suggestions - but that is about all we can do and where it gets a little wacky. Our district is so big that if a vendor doesn’t get a pass from my team, without our knowledge they will go to other departments (curriculum, superintendents, administration, etc.) or even worse, avoid the district offices completely and go directly to schools. In those situations, unqualified individuals - whether it be school administrators (principals, social workers, etc.) or teachers - make the choice to adopt an education app for the entire school or for individual classrooms. Often, personnel in these departments and schools have little understanding of [COPA](https://en.wikipedia.org/wiki/Child_Online_Protection_Act) and [FERPA](https://www2.ed.gov/policy/gen/guid/fpco/ferpa/students.html) laws in regards to the internet and students, nor can they fully conceptualize the ideas of internet based privacy, trust, data mining, and manipulation. 
&nbsp;  

Sadly, being a resource limited public school district, schools and administrators often opt for the “free” education app exposing students to some potentially risky privacy infringement. There are policies in place but the district’s legal team, infrastructure group, and my team are a rather small group of people - about twenty - to monitor the online behavior and app usage of thousands of faculty and tens of thousands of students. Further, our team of digital learning specialists, which knows the most about internet privacy and safety, have little to no say in the drafted policies. Sadly, our efforts have to be more reactive than anything else. Even worse, the problem is only getting bigger and larger, poorer urban school districts from around the country are facing the same issue.
&nbsp;  

As an example, last year a vendor completely circumvented our district office and approached a school to use their free typing game app that eventually became popular in grades kindergarten through five (5-10 year olds). The app had a chat capability with other “players” on the internet and was mining the students chat data. The vendor’s explanation of the app to the school did not include this chat feature and the feature was difficult to find, so most teachers did not know it existed and proceeded to promote the app freely. If review of the app had come through my team, we would have caught it and red flagged it but, again, the app vendor skipped the district offices completely. The app eventually became popular with students, and teachers liked it because it taught students to type. More schools caught on and started to use it. When school based educators discovered the chat feature, many still saw nothing wrong with it nor did they see the potential risks involved. When students went home and started to chat with “friends” they have never met via the app, parents spoke up. The school district then barred the app from being used and it became blocked.
&nbsp;  

However, like with many other institutions, it is not necessarily the fault of the faculties and administrations that they get themselves into these situations. The teachers and administrators I work with have nothing but the best intentions in mind when it comes to the students. They don’t fully understand the ramifications involved with using these free apps. Their lack of knowledge accidentally leads them down the wrong path and vendors capitalize on this ignorance. Educators simply “don’t know what they don’t know” and that is why it is important for education proponents in government, tech companies, and  teams like ours to continue to make educators aware of not only the benefits of the internet in education but also the potential risks involved in the new ways of learning that is evolving in schools.
&nbsp;  

**DSX: Another issue we’ve been seeing talked about extensively is the use of facial recognition. In the US, San Francisco, Oakland, and Somerville have all moved to ban the use of facial recognition software in public spaces. What have you seen in the education space on this front?**  
&nbsp;  

**Nick**: The Smart Schools Bond act of NY provides funding for school districts in NY to purchase technology. With this, the Lockhart school district bought facial recognition software and a camera system to track student movement in schools. Luckily, the NY education assembly committee banned it until 2022, but I feel that administrations still do not have a firm understanding of the implications of having this tool at their disposal, the message it sends to students and parents, and the infringement on student privacy. I would really like to see this get more attention because it is only the beginning of an issue that I see becoming pretty common in the future.
&nbsp;  

**DSX: Do you see any issues where the asymmetry between student and faculty comfort with technology start to pose problems in the schools?**  
&nbsp;  

**Nick**: One thing that concerns me is the increased use of "Student Help Desk" tech teams that are not properly vetted and/or trained, and faculties who depend on them and give them too much access. Because schools are getting tons of hardware, school administrations are beginning to rely on students to help them with the day-to-day infrastructure and maintenance of these systems. I constantly have to reiterate how important it is to not give the students higher level passwords and for there to be consequences should students take advantage of their given status. One of our very bright students who interned with my department during middle school was nearly expelled because his principal trusted him too much. I attended an education technology conference a while back and a group of kids from a student tech team were being featured. During the Q and A, no one asked any questions about security and accountability. When I finally did, neither the presenters nor the students spoke of having anything in place should the students abuse their privileges. There is a desperate need for mandatory digital literacy courses for our teachers to address situations like this.
&nbsp;

*Thanks to Nick for sharing his time and insights. **Want to see more of these types of posts?** Share your ideas with us at info@dsx.us.*



