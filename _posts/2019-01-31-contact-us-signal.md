---
layout: post
title:  "You can contact us on Signal"
date:   2019-01-31 9:00:00
author: Josh |
permalink: /contact-Signal/
---

We’re less than a month into 2019 but there’s a lot going on!

The DSX is busy organizing a number of needs assessments for environmental
justice, women’s rights, and democracy organizations in the U.S.

## Getting in touch with DSX just got easier!

As we mentioned in our [2018 year in
review](https://digitalsecurityexchange.org/2018-review/), it’s easier than
ever to get in touch with the DSX, either to make initial contact or to ask for
help whenever you need it. All methods lead back to our DSX Link suite of tools
that will enable us to respond quickly, using the channel of your choice.

Feel free to contact us using any of these methods (though Signal is preferred):

- Signal (secure): 413-629-8478
- Web form (secure): [https://digitalsecurityexchange.org/request-assistance/](https://digitalsecurityexchange.org/request-assistance/)
- Email (not secure): [help@dsx.us](mailto:help@dsx.us)
- Phone (not secure): 413-629-8478  

## Simplified process

We’re continually working to understand the needs of our community, and we’ll soon be rolling out a stronger, clearer description of what we do and how we do it.

Here’s a sneak preview of our simplified process:

- **Intake**. An organization makes contact with DSX through web forms, Signal (see more about that below), email, or introduction from a trusted contact. DSX staff conduct a brief intake call to understand the organization’s vulnerabilities and needs and what will be required to bring the organization up to a baseline of digital security.  
- **Needs assessment**. Partner providers or DSX staff conduct a multi-day set of staff interviews, trainings, recommendation drafting, and some implementation, bringing the organization up to a baseline of digital security. When possible this process will occur on-site.
- **Onboarding to the DSX Link platform**. Link is our stack of helpdesk tools that make it easy for members of the DSX community to ask for help and stay in touch. Once organizations’ make it through the needs assessment phase, they’re invited to continue to contact DSX directly for help with implementation of recommendations, if they have any urgent needs, or to make contact with a partner provider who can build on the needs assessment work.
- **DSX community**. Sharing knowledge, resources, and methods is a vital part of being a secure and prepared organization. As such, the DSX will be spending the better part of 2019 researching how we can best enable members of the DSX community -- organizations, providers, friends -- to talk to one another, share tips, highlight threats, and keep each other safe. Stay tuned for more on that!
Implementing this new process is going to require new hands on deck. We’re currently raising funds to help support our increased assessment and community-building work.

If you can financially support these important roles, please contact us at [help@dsx.us](mailto:help@dsx.us).

## News

We’re a few weeks in, and already it’s been a busy year in cybersecurity news. Here are a few greatest hits:

- Chinese telecom mega-firm Huawei is in hot water in the U.S., with its [top executive indicted](https://www.nytimes.com/2019/01/28/us/politics/meng-wanzhou-huawei-iran.html?module=inline) for allegedly stealing trade secrets and obstructing an investigation, among other things.
- Turns out a [few big mobile providers](https://motherboard.vice.com/en_us/article/j5z74d/senators-harris-warner-wyden-fcc-investigate-att-sprint-tmobile-bounty-hunters) are selling users’ location data to “bounty hunters” for as little as $300 a pop.
- Thousands of educational, community, and other nonprofit institutions rely on insecure tech that’s at risk of being hacked, as the San Diego Unified School District [discovered](https://boingboing.net/2018/12/27/50-accounts-compromised.html): “After a successful phishing attack that captured over 50 accounts, [hackers stole 500,000 records from the San Diego Unified School District](https://www.zdnet.com/article/hacker-steals-ten-years-worth-of-data-from-san-diego-school-district/), for staff, current students, and past students going all the way back to 2008; including SSNs, home addresses and phone numbers, disciplinary files, health information, emergency contact details, health benefits and payroll info, pay information, financial data for direct deposits.”
- A new [phishing scam](https://krebsonsecurity.com/2019/01/apple-phone-phishing-scams-getting-better/) spoofs Apple incredibly well, down to the company’s logo and name attached to a fake phone number that appears to come from Apple support.  
Last month London police [tested facial recognition technology](https://arstechnica.com/tech-policy/2018/12/londons-police-will-be-testing-facial-recognition-in-public-for-2-days/) that has already found widespread use in the U.S. and around the world.

Hardware authentication, using a physical “key,” is the surest way to secure your online accounts. Now Yubikey, the most popular maker of security keys, is producing [keys that work on iPhones](https://www.wired.com/story/yubikey-lightning-ios-authentication-passwords/).
