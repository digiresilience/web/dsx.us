---
layout: post
title:  "September 2019 DSXpress"
date:   2019-10-02 9:00:00
author: Ingrid |
permalink: /september-2019-dsxpress/
---

We’ve had a productive month here at DSX. Two members of our team just returned from [FIFAfrica](https://cipesa.org/fifafrica/) where they shared a demo of the newly released [CDR Link 1.0](https://digiresilience.org/tech/link.html). We received a lot of positive feedback and interest around the helpdesk. If you would like to discuss how CDR Link might be right for you, or if you are interested in learning more, please get in touch!  

Also this month, our Security Director spoke about staying safe from digital attacks at a [webinar](https://local.nten.org/events/details/nten-oakland-presents-how-to-stay-safe-from-digital-attacks-webinar/) hosted by the NTEN Oakland chapter. Are you interested in having a DSX staff member speak at one of your upcoming events? Let us know!  


See below for more news and highlights.  
 


* * *


**WeWork Insecure WiFi**  
[https://www.cnet.com/news/weworks-weak-wi-fi-security-leaves-sensitive-documents-exposed/](https://www.cnet.com/news/weworks-weak-wi-fi-security-leaves-sensitive-documents-exposed/)  

*Who?* Cowork members  
*What Happened?* **Insecure Network Settings**  
*How?* A NYC WeWork member decided to test the security of the internet provided by his space. To his surprise, he found many sensitive documents and information from various companies sharing the space. The member has since had conversations with WeWork to try and recommend security improvements without any success. Security features are available to some users, but at a hefty price on top of regular membership costs.  

*What could I do?* The article above has some great recommendations for how to best protect yourself when working on any public WiFi, including at your cowork space, in the ‘Possible Fixes’ section. Chief among those is to use a VPN. If you don’t already have a VPN subscription, check out the fantastic [review](https://thewirecutter.com/reviews/best-vpn-service/) by Wirecutter on how to choose one. Further, ensure all your devices - phone and computer - have one installed.   
&nbsp;  

  

**The Guardian SecureDrop Phishing**  
[https://www.bleepingcomputer.com/news/security/phishing-attack-targets-the-guardians-whistleblowing-site/](https://www.bleepingcomputer.com/news/security/phishing-attack-targets-the-guardians-whistleblowing-site/)  

*Who?* The Guardian’s SecureDrop Users  
*What Happened?* **Phishing**  
*How?* A fake site, appearing almost identical to The Guardian’s SecureDrop page, was created to collect code names for anonymous sources. Such information could allow the malicious party to impersonate a Guardian source and gain access to past communications. Further, the site appeared to direct visitors to a malicious Android app, supposedly enabling users to hide their location. If installed, the app allowed the malicious party to monitor the user’s communications, track their location, and more.  

*What could I do?* Continue to stay vigilant for phishing emails and fake sites. When going to a site where sensitive information is concerned - such as your bank or email account settings - navigate to the page directly rather than clicking on links received via email.  

To learn more about using SecureDrop as a source, visit [https://docs.securedrop.org/en/stable/source.html](https://docs.securedrop.org/en/stable/source.html).  
&nbsp;  

  
 
**LastPass Vulnerability**  
[https://arstechnica.com/information-technology/2019/09/lastpass-fixes-bug-that-leaked-the-password-of-last-logged-in-account/](https://arstechnica.com/information-technology/2019/09/lastpass-fixes-bug-that-leaked-the-password-of-last-logged-in-account/)  

*Who?* LastPass Users  
*What Happened?* **Vulnerability**  
*How?* A brilliant [Project Zero](https://googleprojectzero.blogspot.com/) researcher found a new vulnerability in LastPass late last month. The bug would have allowed a maliciously crafted website to steal the login credentials of the last account logged into by the user. The vulnerability was privately disclosed to LastPass, patched by the company, and verified by the researcher before the information was shared publicly.  

*What could I do?* Don’t stop using a password manager! All software can have security vulnerabilities, and the behavior demonstrated here - the vendor listening to the researcher and fixing the issue - is precisely the thing we want to see happen. From a user perspective, there are two steps you should take. First, make sure your password manager is patched and up-to-date. Second, using two-factor authentication (2FA) can protect your accounts even if your password is compromised.  

  
<br>

  
* * *
  

  
**Firefox VPN**  
Firefox [announced](https://nakedsecurity.sophos.com/2019/09/13/mozilla-private-network-vpn-gives-firefox-another-privacy-boost/) beta availability of their desktop browser based VPN in the US.  

**Where’d My Website Go?**  
IREX created this phenomenal [resource](https://ifex.org/whered-my-website-go/) outlining in accessible language what techniques are used to block and deny access to various websites and online content.  

**Journalists and Protests**  
Our friends at Freedom of the Press Foundation updated [this](https://freedom.press/training/blog/how-journalists-can-protect-themselves-while-covering-protests/) great resource detailing how journalists can best protect themselves and their work while covering protests.  
 
**Emotet is back**  
After a quiet summer, the harmful malware known as Emotet is making a resurgence. Read [here](https://blog.talosintelligence.com/2019/09/emotet-is-back-after-summer-break.html) to find some of the tricky tactics used and how to defend your organizations.  

<br>

* * *
  

    

Tech Tips
---------
  


**DNS**. Domain Name System (DNS) is the phonebook of the internet. It allows you to type in a memorable web address, such as etsy.com, and translates that address into an internet protocol (IP) address. This IP address then allows your computer to route your request to the appropriate site you’re trying to reach, as shown in the picture below.    
<img src="../assets/img/Dnsexample.png" width="400" height="350" alt="Image">  
*[Source](https://upload.wikimedia.org/wikipedia/commons/4/4f/Dns-server-upload.png) Б.Өлзий [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)]*  

A historical problem with DNS is that these IP address lookups are done unencrypted, such that anyone listening in can know which websites a user is visiting. In order to make these requests private, DNS over HTTPS (DoH) was introduced. Several companies and products are now supporting this protocol, such as [Cloudflare](https://developers.cloudflare.com/1.1.1.1/dns-over-https/) and [Mozilla](https://blog.mozilla.org/futurereleases/2019/09/06/whats-next-in-making-dns-over-https-the-default/).  

Want to learn more? Visit [here](https://www.zdnet.com/article/how-to-enable-dns-over-https-doh-in-firefox/) to learn how to enable DoH for Firefox.  