---
layout: post
title:  "July 2019 DSXpress"
date:   2019-08-01 9:00:00
author: Ingrid |
permalink: /july-2019-dsxpress/
---

We’re approaching the halfway mark of summer. We’ve been busy continuing to help our community with all their digital security needs as well as demoing our Link platform. 

This past month, Cyber Squad member, Nick, stepped up to assist one of our civil society organizations looking for guidance. He gave them several recommendations, looked into what it would take for them to setup their own virtual network, and gave advice on best practices. 

Are you interested in joining our Cyber Squad and helping organizations in need? Let us know [here](https://dsx.us/become-a-provider/)!

*“It’s easy to feel helpless - like you can’t fight the tide. But remember: small actions can have a huge impact, and one person like you can inspire others to action.”* ~ Celeste Ng


See below for more news and highlights.  
 


* * *


**Malware Installed at the Chinese Border**  
[https://www.vice.com/en_us/article/7xgame/at-chinese-border-tourists-forced-to-install-a-text-stealing-piece-of-malware](https://www.vice.com/en_us/article/7xgame/at-chinese-border-tourists-forced-to-install-a-text-stealing-piece-of-malware)  

*Who?*  Tourists in China  
*What Happened?* **Malware**  
*How?* Border Agents installed the application on tourists entering the country. The app gathers contacts, messages, and an inventory of other apps installed on the phone to upload to a server. Certain keywords and files of interest to Chinese authorities are also sought out by the installed app.  

*What could I do?* It is unlikely that a traveler can avoid installation without causing greater problems such as detention, loss of device, and/or barred entry from the country. If you can bring an alternative device on travel (a device other than your regular, everyday one), you should do so. The less sensitive data on the device, the better. Setting up separate accounts, say on services such as Google and social media, can help protect your regular accounts, too.  

To learn more, visit: [https://www.theglobeandmail.com/report-on-business/small-business/sb-managing/going-to-china-leave-your-laptop-at-home/article4107059/](https://www.theglobeandmail.com/report-on-business/small-business/sb-managing/going-to-china-leave-your-laptop-at-home/article4107059/)

 
  
  
**Internet of Things (IoT) vendor leaking customer data**  
[https://www.zdnet.com/article/smart-home-maker-leaks-customer-data-device-passwords/](https://www.zdnet.com/article/smart-home-maker-leaks-customer-data-device-passwords/)  

*Who?*  Consumers with Orvibo IoT Devices  
*What Happened?* **Security Misconfiguration**. The company left a server connected to the internet without a password, allowing researchers and others to view sensitive customer data.  
*How?* The misconfiguration allowed internet users to look through information about Orvibo SmartMate customers. Data included email addresses, usernames, hashed passwords, and even password reset codes. With this information, a malicious user could potentially take control of and/or lock a family out of their devices.  

*What could I do?*
Research smart products before purchase to ensure the company has a good track record of creating devices with security and privacy in mind. When you do install a smart device in your home, set a strong password and use available security configurations.  
  
To learn more, visit [https://www.iotforall.com/how-to-secure-iot-devices/](https://www.iotforall.com/how-to-secure-iot-devices/)
  
  
 
  
**Kazakhstan Government tries to spy on citizens’ internet traffic**  
[https://www.privateinternetaccess.com/blog/2019/07/kazakhstan-tries-and-fails-to-mitm-all-of-its-internet-users-with-rogue-certificate-installation/](https://www.privateinternetaccess.com/blog/2019/07/kazakhstan-tries-and-fails-to-mitm-all-of-its-internet-users-with-rogue-certificate-installation/)

*Who?*  Kazakhstan citizens  
*What Happened?* **Man in the Middle**. The Government attempted to have all citizens install a certificate that would allow them to intercept, decrypt, and read all internet traffic.  
*How?* The government ordered all internet service providers (ISPs) to direct internet traffic to a site where the certificate could be downloaded.  

*What could I do?* Use a circumvention tool, such as Psiphon, to ensure you get the access you want.  

To learn more, visit [https://psiphon.ca/](https://psiphon.ca/)  
  
<br>

  
* * *
  

  
**Cell Site Simulators** 
A great new [article](https://www.eff.org/wp/gotta-catch-em-all-understanding-how-imsi-catchers-exploit-cell-networks) from EFF explains how stingrays or IMSI-catchers work to intercept and listen in on your mobile communications.

**Email Pixel Tracking**
This [article](https://mikeindustries.com/blog/archive/2019/06/superhuman-is-spying-on-you) on the email client, Superhuman, details how tech developers can miss important privacy by design concepts. Want to know how to help thwart this practice? Read Chad Loder’s great Medium [post](https://medium.com/@chadloder/how-to-disable-superhumans-email-tracking-pixel-ba4f9ccdf731). To learn more about the practice of email tracking, visit [https://freedom-to-tinker.com/2017/09/28/i-never-signed-up-for-this-privacy-implications-of-email-tracking/](https://freedom-to-tinker.com/2017/09/28/i-never-signed-up-for-this-privacy-implications-of-email-tracking/).
  
**Google Alternatives**
Looking to not have all your eggs in one basket? This  [article](https://www.techspot.com/news/80729-complete-list-alternatives-all-google-products.html) highlights alternatives to the tech giant.

<br>

* * *
  

    

Tech Tips
---------
  


**Ransomware**. People tend to think encryption equals protection and security - but that depends on who holds the keys! Ransomware is when a malicious entity encrypts your data and won’t unencrypt it without payment. The entry point for such an attack may be a malware-laden email attachment or an unpatched vulnerability on an organization's network. This type of attack has been widespread, seen in Georgia, [Baltimore](https://www.baltimoresun.com/maryland/baltimore-city/bs-md-ci-ransomware-email-20190529-story.html), and [Louisiana](https://nakedsecurity.sophos.com/2019/07/28/ransomware-hits-louisiana-schools-state-of-emergency-declared/) just this year. As more and more of our world becomes internet connected, the potential for ransomware expands.

Want to learn more? The end of this [article](https://www.zdnet.com/article/ransomware-why-cities-have-become-such-a-big-target-for-cyberattacks-and-why-itll-get-worse-before-it-gets-better/) provides some simple steps you can take to protect yourself and your organization. Additionally, this [article](https://www.zdnet.com/article/no-more-ransom-project-has-prevented-ransomware-profits-of-at-least-108-million/) covers the No More Ransom project and its three year effort to help victims avoid paying.