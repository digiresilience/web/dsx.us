---
layout: post
title:  "Conference Tips Checklist"
date:   2019-06-25 9:00:00
author: Ingrid |
permalink: /conference-tips-checklist/
---

Conference season is in full swing! While you’re planning what to pack, who to meet with, and what sessions to attend, run down this short checklist to make sure your digital security is ready, too! Add a bookmark to this page or print it out so you can easily refer back to it during your trip and before attending future conferences.  
&nbsp;  
&nbsp;  

  

## <span style="color:blue">**Before**</span>


<span style="color:lightsalmon">*"An ounce of prevention is worth a pound of cure." Benjamin Franklin*</span>

**Devices** *What am I going to bring? Will I use it/need it for the conference? Bring what you need to do your work and be productive, but no more.*  
<p style="margin-left: 40px">&#9744;  Pick the devices you'll bring.</p>
<p style="margin-left: 40px">&#9744;  Apply privacy screens to your devices to avoid shoulder surfing.</p>  
<p style="margin-left: 40px">&#9744;  Enable passcode/passwords to unlock devices.</p>

**Data** *What documents, photos, and videos do I have on the devices I’m bringing with me? Maybe that super secret project report can be transferred off to an external drive and doesn’t need to be on your laptop for the conference.*  
<p style="margin-left: 40px">&#9744;  Review data stored directly on your device.</p>
<p style="margin-left: 40px">&#9744;  Offload any sensitive or unneeded files to the cloud or offline storage.</p>

**Software and Accounts** *What software do I need to install and configurations to set in order to be productive and secure while I’m traveling?*  
<p style="margin-left: 40px">&#9744;  Install a VPN to use over open conference, hotel, and airport WiFi networks (insecure WiFi networks can allow others to easily watch your internet activity). Not sure which one to use? Look <a href="https://thewirecutter.com/reviews/best-vpn-service/">here</a> for recommendations.</p>
<p style="margin-left: 40px">&#9744;  Use a secure method to chat with people, like <a href="https://signal.org">Signal</a> or WhatsApp.</p>
<p style="margin-left: 40px">&#9744;  Turn on two-factor authentication for work <em>and</em> personal email, collaboration, and file sync accounts. Here's a <a href="https://medium.com/@mshelton/two-factor-authentication-for-beginners-b29b0eec07d7">good tutorial</a>.</p>
<p style="margin-left: 40px">&#9744;  Do you keep hitting "remind me later" for software updates? <strong>Don't</strong>. Make sure you've applied all your updates since unpatched software = potential for compromise. Use auto-update settings, where available, to set it and forget it.</p>

**Backups** *When is the last time you made a copy of the data you rely on for your important work? Using Time Machine or similar apps can prove critical if your device is lost, stolen, or compromised.*  
<p style="margin-left: 40px">&#9744;   Create a backup for each device you're bringing.</p>

## <span style="color:blue">**During**</span>

<p style="margin-left: 40px">&#9744;   <strong>Use a VPN</strong>. When connecting to airport, conference, and hotel WiFi, use your installed, trusted VPN to ensure no one snoops on your communications.</p>
<p style="margin-left: 40px">&#9744;   <strong>Don't plug it in</strong>. Vendors love to hand out free USB Drives. Don’t take the bait! As this <a href="https://www.vice.com/en_us/article/pajv5k/john-deere-promotional-usb-drive-hijacks-your-keyboard">John Deere story</a> shows, USB drives can deliver malware in an instant. Also, beware of unknown cables and public charging stations, which can be used to compromise your device.</p>
<p style="margin-left: 40px">&#9744;   <strong>Know where your devices are</strong>. Be mindful of leaving electronics unattended! Beware of theft or malicious tampering. Make sure your devices are protected by locked screens and strong passcodes.</p>
<p style="margin-left: 40px">&#9744;  <strong>Be aware of public computer use & public conversations</strong>. Stay mindful of who may be listening or looking in when discussing or working on sensitive topics.</p>
<p style="margin-left: 40px">&#9744;   <strong>Protect against phishing</strong>. If you receive an unexpected email attachment or a message that doesn't seem quite right, make sure it's not a phishing attack. <strong>Don't click on weird links in emails.</strong> If an email asks you to click on a link to access your account, <strong>DON'T</strong>. Go directly to the account instead.</p>

## <span style="color:blue">**After**</span>
<p style="margin-left: 40px">&#9744;   Is something strange going on with your computer? Does your phone seem "different"? Any other digital security incident you need help with? <a href="https://dsx.us/request-assistance/">Contact</a> the DSX Helpdesk to discuss.</p>
 
 
 
 &nbsp; 
 &nbsp; 
* * *


Just following these small actions can go a long way to strengthening your digital security posture and setting you up to have a safe, enjoyable trip! Traveling to a destination that requires more specialized recommendations? Reach out via our helpdesk to discuss. 

Want some additional resources to help? Check out these links, below:
[https://securityplanner.org/](https://securityplanner.org/)  
[https://ssd.eff.org/en#index](https://ssd.eff.org/en#index)  
[https://freedom.press/training/your-smartphone-and-you-handbook-modern-mobile-maintenance](https://freedom.press/training/your-smartphone-and-you-handbook-modern-mobile-maintenance)