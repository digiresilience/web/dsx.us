jQuery(document).ready(function($){
  initForm({
    endpoint_config: 'https://help.digitalsecurityexchange.org/api/v1/form_config',
    form: '#assistanceForm',
    form_title: 'Request Help Submission',
    email_system: 'help+requesthelp@dsx.us',
  });
  $("input[name='service1'],input[name='service2'],input[name='service3']").each(function(){
    $(this).after("<span class='connector'></span>")
  })
});
