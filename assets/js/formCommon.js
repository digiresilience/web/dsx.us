function initForm(formConfig) {
  const {
    endpoint_config,
    form,
    form_title,
    email_system
  } = formConfig;

  function fingerprint() {
     return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAKu0lEQVR4nO3af3BV5Z3H8U8IBDRIMCLI8qvQrVZLWyyI7RIKiChoUel4bfmdnOecJ0FJAZeNiEojWNduYauB9J4nAckiaUq2FPnVMSgESbjnQNNWuzK1Xfyd6TitI670D2bWNvtHbhjYUgiEmVum79fMmfne5Lnn+Z5z5vnMvedcCQCAv1VtVm1/j1umzzuAC5Dp4CCwAHRapoODwALQaZkODgILQKdlOjgILHSdSY6UDVv/6v+DZOEZ64shcC/KJG/8i/pSEVSNknVHM91GZ2U6OAgsdN1ZA6stS4F7/y/ri6C0oqcC957UlnVafSkhsC6JLdPnHRdTe2C91f/hsjWTfjj6uAL3toKq2yVJ1m1T4P6swL02/El7uKMeWz1luWz4XwrcWgVhowL3G/nV4yVJifocWfecbPiGbPiWrKtV4t8vkyQF7n1ZVyBJKg4ny7p6Sbpn19Dn8pc9ulvWLRxbNeWjvKUrW9v7cJ8oSN6X7sWXDX8rG76lwO1XUeWQ9n1WjZINX5F1/6bA7ZcNj8ivvkU23KrAvarArT2vcWebS5KC8GEF7r30vh6RdUe9Jo0wkapOO6/F4RwF7k3ZsFVBuFmlFT3T+04ocK/Jul+39+E+e979XaBMBweBha5rD6wTg1YsWmIi1ajYWQXuoCTJun6y4Qm1KWt+U86WjnrW3j47ZV2b/HCKJCmomqbA/UaS5If3Kgj3SG1ZKi/vJutWyyTHSZKKw6ky6/Pb3xM+JeuKbYt63LntH38lmyy2LephIm1Um7IUVN2VXtiXq+jZq2XDEypJfir93vUKXHhK/39SUPVP6Z5rFbjXVFrRU4Ube8mGx1VYeU2nx51tLr/6egXumKwbmD622jMGVknyU7LhB/KrhytRn60g3Cnrlsm6obLh/yioujbdwwOyLj6v4+iCTAcHgYWuM8mRCtzH5qCG+ZHWT9t2XdnE2puPm1gbpv3k07fIhif8SI8UNnV/YdzGCZ/4kb4zpzF3f0HNV0/MS2msl9K6ogPdFoxxU9vmHehVM/Lpe+bIhq3Z91dMn3+ge5mJVO1HWm9SWpo4ohxJMrE2XbXs4ddk3ae9Q/ri6HBaa0dtIj00fduQwombx/5x1r68rSbWBj/W8zP39r5FkrxYt92zc/DeW380qtXEenLEE16BbPiB16QRfqSKsdVTdkyqHfNzE6vSj/WF0ck7P/jGnvy6u3cMfbwz40xKxR2fCP//XPllK/5ZgdsuSSalRKKhf8P4moKPvEj3nRZYQTLoGOc1a1LRwexnvVS3TbfWjdqSXbJ2Z3rfBV4qOzmuZvyf5zf1WH3Dmm9O7nR/FyjTwUFgoevS97DSgfWTSZtv9GTDVpPS1HkvX7ZWNjwxO1afwubuuzrqOft771Xg3k4vrp3BYY1S4P44YfNXZphYoaxLDH689Bdj3NQTsuGzSpT3NrE8P9L1knTn1mGTch54+k1JmvlSn/uHPVHyO0kykeYXvpx9h4KwWdb5klQU6WY/1rqchd8v77lwzc+nbb3+w8sXP/WGbPiSF6n0np1DVilwb5uDGmYibR9c/q11sm61SWmpH2tdt5LK1y8r/e6XZzfmvtBz4Zp3zzXOxKrvveTJ75xprnE147fLupqilzXExKobtOJbk2WTR01KS08LLOvKZF2NH2uAiVTrxxpQ3qZuk+tu3DPymcS+ogO62o+0ZV5Kg2TD43dsvdb/xp78jZ05DhOrflaTrryQS53p4CCw0HWnB9aWjtf+IQ2f15RT+1cDy4bHvSaNMLHq2r+yuLbeJeX9TaQdtkV5RbFumH8gp27gikWHu5c8s+y0Oa1LKAjXS9Ldu4bt6vNgea0k+bG+33fpt59R4Ookybaon4m0ceiq0mJZ9ytZl5c4ohz54TzZ8KWilCbct6df8pSFXivrymXdaj/WHD/SAtnwdRWHY2btzavPW7qy9Vzjvv7TQXvy/uXx188019Qfj9wt67Z5kaaZSI8qqPqarDtaFOlLp3/CqvIUhLuLUrrdRHpUC35wpYoqh/QqXRNkF6/b4cea4qX0WPqr3p8GLzH5c/fn7uux4Jl3ztWfH8t5P9OIC7nUmQ4OAgtdd0pgmUg1p74ubO5Rq8B9clPlxGuKmrN3dNTpwPrfYasWPGBibVBQNVc2PCJJM3b/wy/zH3pkjdSW5cUqmFB786szdg1+xaS06PIlT02XWZ+vIKySH870mnXF7T/+3K876kTDgN2y4W81u6JPeZu6mUjf9WLdJusWKgh3qk1Z32zo603Y/OW3x//HuN95KblEQz93MrBibehY6CalWSaW17HQZ76Yt6Xv0vLWc42bvn1E01Vly/edaa57G/pXKXDHvr57YLF3sNuDsuFWWXe0MNZnTKQqWTdLfvUAlSQHybqPpm29brGJui1REP6nrFsmv3qwAnfsru3DF5qUFsm6xQrCA5I0uzH3hdzF//reufrzIyULX9ZnLuRSZzo4CCx03VkCy0SqURDu6fHA08dmN17R2FEnGq7+mQL331cvL6u+rf7zH8sm35VJjkvUK6ewuXtDz9I1L8iG76afEtbfVDnxGhPpiRFP+sdkXYGsO6qSZH8vVsEXKmb8oaP+UvKOIwrcx7Jh63Xfm/PhqMq7PpJ15e03wl08cMXidybW3vRu37IVkxS4969dPeeHFzuw5h/otfHKssdeOdNcJtITsm7l0FULPhzjpv1e1i2RDd8KDusmE6nqtKeg1iWGrFz4+9Hh1OOy7jmVVvRMNKp3/+XL5w1ddf+bY9zUPyhwL8r7wYhEvXLm7s/dl11S8Q6BRWChE04G1Blez21Qrh/r+cJG9ZrboNw5jb0bsxesfcNr0giT0vNerAJJ8mJNMrEqJclEmm5SmqU2ZalNWSbWIi+lGZI0v0mjvWZdYVJaaA7rq5J0al14UKO8lJxt0eWn9liU0tf8WI9J0txXlevHWmlife9kv7E2nOy/Y6GndSz0zo4721z+IQ03sepMSvmJemWbWA+f+pWwKKUJhb9U33mHdJWJ9CPboqHlbermx1rmx7p3ZqP6mVh1tkUDJanooO42kZ46n+M4v6vbLtPBQWDhojlbYElSesHWec26LtEwwN265YvHTUqT/VjOxPJMrNCP9WzHjXXbojyT0or039abSA8lUrpMan9KWBTrBhOpqrBRfSXp1No/pG/7kbaYSDUnt5Rm2RbleZFW+5GSJtKqwpQ+a2JtMpHmX+zAOttckuRFmm1S2uRHSvqx7vYjrT+5z/TxSe1PA02kjSalTV6kB22LekiSd1BfMZHWntz/YV1z8rwTWAQWLqL0L7zP+INJ/M3KdHAQWMgoAuvSkungILCQUQTWpSXTwUFgAei0TAcHgQUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIBL1v8Be5wa2KOS9hQAAAAASUVORK5CYII=';
  }

  const $form = $(form);
  $form.parsley();

  window.Parsley
    .addValidator('multipleFields', {
      requirementType: 'string',
      validateString: function(value, requirement) {
        if (value || $('#email').val() || $('#signal').val()) {
          return true;
        }
        else {
          return false;
        }

      },
      messages: {
        en: 'Please fill in at least one contact field -- either Phone, Email, or Signal'
      }
    });

  function submitForm(config) {
    $.ajax({
      method: 'post',
      url: config.endpoint,
      data: getParams(config.token),
      cache: false,
      contentType: false,
      processData: false,
    }).done(function(data) {

      if (data.errors) {
        console.log('ajax submission errors')
        $.each(data.errors, function( key, value ) {
          console.log('error', key, value);
        })
        return
      }

      // ticket has been created
      window.location.href = '/thank-you/';

    }).fail(function() {
      $form.find('button').prop('disabled', false)
      console.log('ajax request for form submission failed')
    });
  }

  function getParams(token) {
    var formData = new FormData($form[0])

    let body = ''
    for (var p of formData) {
      const [key, value] = p;
      body += `\n${key}: ${value}`
    }

    const zammadFormData = new FormData();
    zammadFormData.set('name', formData.get('name'))
    zammadFormData.set('email', email_system)
    zammadFormData.set('body', body)
    zammadFormData.append('fingerprint', fingerprint())
    zammadFormData.append('token', token)
    zammadFormData.append('title', form_title)

    return zammadFormData
  }

  $form.parsley().on('form:submit', function (f) {
    const params = {fingerprint: fingerprint()};

    $.ajax({
      method: 'post',
      url: endpoint_config,
      cache: false,
      processData: true,
      data: params
    }).done(function(data) {
      console.log('config:', data)
      submitForm(data)
    }).fail(function(jqXHR, textStatus, errorThrown) {
      console.log('result', textStatus)
      if (jqXHR.status == 401) {
        console.log('error', 'Failed to load form config, feature is disabled!')
      }
      else {
        console.log('error', 'Failed to load form config!')
      }
    });

    // always return false to disable default submit behavior
    return false;
  })
}
